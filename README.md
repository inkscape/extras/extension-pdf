# PDF Output

This extension takes an SVG from Inkscape and outputs a PDF/x3 pdf file.

This will follow the functions available in the libcapypdf library and will depend on pre-processing steps in inkscape's extensions system to turn the svg from inkscape-svg to a something simpler which can be rendered into PDF without too many issues.

If you pass an SVG file without these steps you may find reduced functionality.

# Installing

To use this extension you will need to install the CapyPDF python binary wheel. Wheels are available for windows and linux at this time.


- OS tested on PopOS 22.04 and Arch Linux (6.8.1 Kernel)
- Install latest gcc from PPA (13.1 and 13.2 are tested)
- Make gcc the default library by using alternatives command

**Generating the capypdf wheel file:**

```
# Clone capypdf
git clone https://github.com/jpakkane/capypdf
cd capypdf

# Install a pythonenv/env/venv using python3.10/3.11
python -m venv venv
source venv/bin/activate

# Install the latest version of meson, this is because meson has a hard coded set of strings about what C++ versions are possible.
pip install meson-python
./venv/bin/meson setup builddir

# Compile
cd builddir
ninja

# To generate the Python Wheel
cd ..
pip install build
python -m build . --wheel

# Now you have the wheel file, you can install it somewhere else or in another virtualenv.
```

# Potential repairs

 * Added --debug to pyproject.toml to include debug symbols in wheel
 * Change to pyproject.toml to fix README file location and C++latest is not valid, changed to c++23
 * Changes to capypdf.py to load the library from the wheel location, handle OSError instead of NotFound
 * Remove library load override in x3gen.py and depend on virtualenv wheel to find the library
 * Compile icc color profile using colprof from argyle package, generating an icc that can be included in the package


Now you can clone extension-pdf repo and work with it. Create a virtual
environments for your python packages.


**fontconfig**: You might have trouble with `Python-fontconfig`, the work around for now is to
install it from
`git+https://github.com/ldo/python_fontconfig@f23bf2f70362ee145bac26d9bb4d3ab937298203`


You will also need to `pip install inkex`.

A more automated and cleaner version of the install process, at least for
development is coming soon.



