#!/usr/bin/env python3
# coding=utf-8
#
# Copyright (C) 2023 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
#
"""
Raster, color and icc image utilities
"""

import os
import sys

from io import BytesIO
from PIL import Image, ImageOps, ImageCms
from base64 import decodebytes

from inkex.transforms import Transform

# disable the PIL decompression bomb DOS attack check.
Image.MAX_IMAGE_PIXELS = None

class HrefData:
    """A mixin class for managing href data"""
    name = property(lambda self: self.get_id())
    ref = property(lambda self: self.get("href") or self.get("xlink:href"))
    extensions = {None: 'tmp'}

    def tempdir(self):
        if not hasattr(self, '_tempdir'):
            if hasattr(self.root, 'tempdir'):
                self._tempdir = self.root.tempdir
            else:
                raise ValueError("Need to write code to generate new temp dir here")
        return self._tempdir

    def tempfile(self, mimetype=None):
        ref = self.ref
        if not mimetype and ref.startswith("data:"):
            mimetype = ref[5:ref.index(";")]
        return os.path.join(self.tempdir(), f"{self.name}.{self.extensions[mimetype]}")

    def get_filename(self):
        ref = self.ref
        if ref.startswith("file://"):
            # This doesn't handle relative references, is this a problem?
            return ref[7:]
        if ref.startswith("data:"):
            return self.save_data()
        return None

    def get_data(self):
        if not self.ref.startswith("data:"):
            fname = self.get_filename()
            if os.path.isfile(fname):
                with open(fname, "rb") as fhl:
                    return fhl.read()
            return None

        data = self.ref[5:]
        (mimetype, data) = data.split(";", 1)
        (base, data) = data.split(",", 1)
        if base != "base64":
            sys.stderr.write("Bad data block encoding")
            return None

        return decodebytes(data.encode("utf-8"))
    
    def save_data(self):
        filename = self.tempfile()
        if not os.path.isfile(filename):
            with open(filename, 'wb') as fhl:
                fhl.write(self.get_data())
        return filename


class ImageData(HrefData):
    """More specific tools for images"""
    extensions = {
        'image/png': 'png',
        'image/jpeg': 'jpeg',
        'image/tiff': 'tiff',
    }

    @property
    def img(self):
        if not hasattr(self, '_img'):
            self._img = Image.open(BytesIO(self.get_data()))
        return self._img

    def get_filename(self):
        """Re-save file with the correct color encoding and orientation"""
        img = ImageOps.flip(self.img)

        # Convert to CMS color managed mode
        #https://pillow.readthedocs.io/en/stable/reference/ImageCms.html

        with open(self.tempfile(), 'wb') as fhl:
            img.save(fhl)
        return self.tempfile()

    @property
    def img_translate(self):
        return Transform(translate=(
            self.to_dimensionless(self.get("x", 0)),
            self.to_dimensionless(self.get("y", 0))))

    @property
    def img_scale(self):
        return Transform(scale=(
            self.to_dimensionless(self.get("width", 1)),
            self.to_dimensionless(self.get("height", 1))))
